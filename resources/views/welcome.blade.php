<!DOCTYPE html>
<html>
	<header>
		<!-- Nama Pada Bagian Tab -->
		<title> Terima Kasih! </title>

		<!-- Character Set UTF-8 covers almost all characters in the world -->
		<meta charset="utf-8">

		<!-- Make Wep Page look good on all devices -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</header>

	<body>

		<div>
			<h1> SELAMAT DATANG {{$fname}}  {{$lname}}! </h1>
			<h2> Terima kasih telah bergabung di SanberBook. Social Media kita bersama! </h2>
		</div>
		
	</body>
</html>