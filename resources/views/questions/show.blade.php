@extends('layouts.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4>{{ $details->judul }}</h4>
        <p>{{ $details->isi }}</p>
    </div>
@endsection