@extends('layouts.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Create Question</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/pertanyaan" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputTitle">Judul Pertanyaan</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="judul" placeholder="Masukkan Judul">
            @error('judul')
                <div class="aler alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputContent">Isi Pertanyaan</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name="isi" placeholder="Masukkan Pertanyaan">
            </div>
            @error('isi')
                <div class="aler alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection