@extends('layouts.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Edit Question {{ $edit->id }}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/pertanyaan/{{ $edit->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputTitle">Judul Pertanyaan</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="judul" value="{{ old('judul', $edit->judul) }}" placeholder="Masukkan Judul">
            @error('judul')
                <div class="aler alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputContent">Isi Pertanyaan</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name="isi" value="{{ old('isi', $edit->isi) }}" placeholder="Masukkan Pertanyaan">
            </div>
            @error('isi')
                <div class="aler alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Change</button>
        </div>
    </form>
</div>
@endsection