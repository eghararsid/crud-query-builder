<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        return view('questions.view', compact('pertanyaan'));
    }

    public function create() {
        return view('questions.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate(
            [
                'judul' => 'required',
                'isi' => 'required'
            ]
        );

        $query = DB::table('pertanyaan')->insertGetId(
            [
                'judul' => $request['judul'],
                'isi' => $request['isi']
            ]
        );

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Ditambahkan!');
    }

    public function show($pertanyaan_id) {
        $details = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        // dd($details);
        return view('questions.show', compact('details'));
    }

    public function edit($pertanyaan_id) {
        $edit = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();

        return view('questions.edit', compact('edit'));
    }

    public function update($pertanyaan_id, Request $request) {
        $request->validate(
            [
                'judul' => 'required',
                'isi' => 'required'
            ]
        );

        $query = DB::table('pertanyaan')
                    ->where('id', $pertanyaan_id)
                    ->update(
                        [
                            'judul' => $request['judul'],
                            'isi' => $request['isi']
                        ]
                    );

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Diperbarui!');
    }

    public function destroy($pertanyaan_id) {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Telah Dihapus!');
    }
}
